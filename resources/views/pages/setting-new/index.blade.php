@extends('layouts.app')
@section("content")
    <setting-new-index-component
        props-urls='{!!
                        json_encode([
                            "index" => route("home",["api"=>1, "serial_key" => request()->query("serial_key")]),
                            "redirect_url" => route("home"),
                            "store" => route("store"),
                            "status" => route("status"),
                            "serial_key" => request()->query("serial_key"),
                        ],JSON_HEX_APOS)
                    !!}'
        props-data='{!!
                        json_encode([
                            'shop' => \Auth::user()->makeHidden(['theme_content']),
                            'handprint_api_url' => config('app.handprint_api_url'),
                            'sso_login_url' => config('app.sso_login_url'),
                        ],JSON_HEX_APOS)
                    !!}'
    >
    </setting-new-index-component>
@endsection
