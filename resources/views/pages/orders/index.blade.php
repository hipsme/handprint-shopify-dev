@extends('layouts.app')
@section("content")
    <orders-component
        props-urls='{!!
            json_encode([
                "orders" => route("api.orders", ["id" => \Auth::user()->id]),
            ],JSON_HEX_APOS)
        !!}'
        props-data='{!!
            json_encode([
                'shop' => \Auth::user()->makeHidden('theme_content'),
            ],JSON_HEX_APOS)
        !!}'
    >
    </orders-component>
@endsection
