@extends('layouts.app')
@section("styles")
    <style>
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        .active, .accordion:hover {
            background-color: #5c6ac4;
            color: #ffffff;
        }

        .panel {
            padding: 0 18px;
            display: none;
            background-color: white;
            overflow: hidden;
        }
    </style>
@endsection
@section("content")
    <div class="Polaris-Page installation-guide">
        <div class="Polaris-Page__Content">
            <div class="Polaris-Layout">
                <div class="Polaris-Layout__Section">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header" style="text-align: center;">
                            <h2 class="Polaris-Heading"> User Guide </h2>
                        </div>
                        <div class="Polaris-Card__Section">
                            <button class="accordion">
                                1. Create your Handprint account <a href="https://handprint.tech/" target="_blank">Here</a>
                            </button>
                            <div class="panel">
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/1.png" }}" style="width:100%;">
                                </p>
                            </div>

                            <button class="accordion">2. Verify your account from your email and you'll be able to enter into our Handprint dashboard!</button>
                            <div class="panel">

                            </div>

                            <button class="accordion">3. On the left, select the E-commerce plugin option</button>
                            <div class="panel">
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/2.png" }}" style="width:100%;">
                                </p>
                                <ul>
                                    <li>Here you can Choose projects, Define your pledge, Set a cap budget, Personalize your content and Style your Plug-in.</li>
                                    <li>Dont forget to click "Save" at each stage</li>
                                </ul>
                            </div>

                            <button class="accordion">4. Click the “Go Live” button on the top right hand corner</button>
                            <div class="panel">
                                <ul>
                                    <li>A screen will pop up asking you to key in your Credit Card details.</li>
                                    <li>This is necessary for us to create a billing account to make your contribution seamless. Your card details will not be
                                        stored by us but by Stripe.</li>
                                </ul>
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/4.png" }}" style="width:100%;">
                                </p>
                            </div>

                            <button class="accordion">5. Go to your Shopify backend to install the Handprint Plug-in</button>
                            <div class="panel">
                                <ul>
                                    <li>Under Apps, select “Shop for apps”</li>
                                </ul>In the search bar, type “Handprint” and hit search
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/5.png" }}" style="width:100%;">
                                </p>
                                <ul>
                                    <li>In the search bar, type “Handprint” and hit search</li>
                                </ul>
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/5_1.png" }}" style="width:100%;">
                                </p>
                            </div>

                            <button class="accordion">6. There should only be 1 Handprint, ensure that you are seeing the logo below.</button>
                            <div class="panel">
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/6.png" }}" style="width:100%;">
                                </p>
                                <ul>
                                    <li>Once you are certain it is the correct one, click on “Add app”</li>
                                </ul>
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/6_1.png" }}" style="width:100%;">
                                </p>
                            </div>

                            <button class="accordion">7. Hit the “Install app” button and give it a few seconds</button>
                            <div class="panel">
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/7.png" }}" style="width:100%;">
                                </p>
                            </div>

                            <button class="accordion">8. Your widget should reflect the one that you had configured in your Handprint account previously</button>
                            <div class="panel">
                                <ul>
                                    <li>The serial key should already be provided but if not you can find it when you select “e-commerce plug-in” in your
                                        Handprint account</li>
                                </ul>
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/8.png" }}" style="width:100%;">
                                </p>
                                <ul>
                                    <li>Copy it from your Handprint account and paste it in your Shopify account. <b>Remember to click Save on the Shopify Admin to save your settings!</b></li>
                                </ul>
                                <p>
                                    <img src="{{ config('app.url')."/images/User_Guide/8_1.png" }}" style="width:100%;">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("script")

    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>
@endsection


