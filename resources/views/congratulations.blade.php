<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
	<title>Congratulations</title>
	<style type="text/css">
	    @font-face{font-family:Gilroy-bold;src:url(https://staging.handprint.tech/themes/front/default/assets/fonts/Gilroy-Bold.eot);src:url(../fonts/Gilroy-Bold.otf) format('opentype'),url(https://staging.handprint.tech/themes/front/default/assets/fonts/Gilroy-Bold.woff2) format('woff2'),url(../fonts/Gilroy-Bold.woff) format('woff'),url(../fonts/Gilroy-Bold.ttf) format('truetype'),url(https://staging.handprint.tech/themes/front/default/assets/fonts/Gilroy-Bold.svg) format('svg')}body{margin:0;padding:0;-webkit-box-sizing:border-box;box-sizing:border-box}.hp_congratulation{background-image:url(https://staging.handprint.tech/themes/front/default/assets/img/bg.svg);background-repeat:no-repeat;background-position:center center;background-size:cover;background-attachment:fixed;font-family:Gilroy!important;height:100vh}.hp_congratulation .signup_new_bar .progress-bar-div{text-align:center;padding-top:5rem}.hp_congratulation .signup_new_bar .progress-bar-div img{width:100%;height:87px;display:block;margin:auto}.hp_congratulation .signup_new_bar .progress-bar-div h3{margin:0;font-size:32px;line-height:39.62px;font-family:Gilroy-bold;color:#000;padding-top:70px}.hp_congratulation .signup_new_bar .progress-bar-div p{margin:0;font-family:Gilroy-bold;font-size:20px;line-height:30px;color:#000;padding-top:30px;max-width:485px;margin:0 auto;padding-bottom:50px}.hp_congratulation .signup_new_bar .progress{background:#f7fdfb;justify-content:left;border-radius:100px;align-items:center;position:relative;padding:0;display:flex;height:32px;width:680px;border:1px solid #97a3ad;margin:auto}.hp_congratulation .signup_new_bar .progress-value{animation:load 3s normal forwards;box-shadow:0 10px 40px -10px #fff;border-radius:100px;background:#1dca99;height:32px;width:0}@keyframes load{0%{width:0}100%{width:100%}}@media(max-width:650px){.hp_congratulation .signup_new_bar .progress{width:100%}.hp_congratulation .signup_new_bar{padding:20px}}
    </style>
</head>
<body>
    <div class="hp_congratulation">
    	<div class="signup_new_bar">
            <div class="progress-bar-div">
                <img src="https://handprint-local.s3.ap-southeast-1.amazonaws.com/images/default/logo-banner.svg" alt="image" title="Handprint Logo" />
                <h3>Congratulations!</h3>
                <p style="font-size: 24px;">You installed the Handprint plugin and are ready to create positive impact.</p>
               <div class="progress">
    			  <div class="progress-value"></div>
    			</div>
             </div>
        </div>
    </div>
    <script>
        let i = 0;
        var myVar = setInterval(function() {
            i++;
            window.document.getElementsByClassName(
                "progress-value"
            )[0].style = `width:${i}%;`;
            if (i == 100) {
                clearInterval(myVar);
                window.location.href = "{!! config('app.handprint_signup_url') .'/set-user-login/' . $user->api_user_id . '?type=shopify&store=' . $user->name !!}";
            }
        }, 25);
    </script>
</body>
</html>