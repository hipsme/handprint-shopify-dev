<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('app.name', 'Handprint') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-url" content="{{ env('APP_API_URL') }}">
    <!-- Fonts -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.5.0/dist/styles.css" />
    @yield('styles')
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css" />
    <link rel="stylesheet" href="https://staging.handprint.tech/themes/front/default/assets/css/widget-sp.css" type="text/css" />

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ \Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
    <script>window.Laravel = {csrfToken: '{{ csrf_token() }}'}</script>

</head>
<body>
<div id="app">
    @include('layouts.nav')
    <div class="">
        @yield("content")
    </div>
</div>
</body>

<script src="{{  asset(mix('js/app.js'))  }}"></script>

@yield('script')

</html>
