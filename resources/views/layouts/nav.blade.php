<div class="Polaris-Card">
    <div>
        <ul role="tablist" class="Polaris-Tabs">
            @foreach(adminNav() as $key => $val)
            <li  role="presentation" class="Polaris-Tabs__TabContainer">
                <a href="{{ $val['link'] }}" class="Polaris-Tabs__Tab {{ $val['active'] }}">
                    <span class="Polaris-Tabs__Title">{{ $val['title'] }}</span>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</div>
