require("./bootstrap");

if ($$$("#panther_label_data").length > 0) {
    var shop_data = JSON.parse(
        document.getElementById("panther_label_data").innerHTML
    );


    const host =
        typeof shop_data.app_url !== "undefined"
            ? shop_data.app_url
            : "https://dashboard.handprint.tech";

    const css_host =
        typeof shop_data.app_url_css !== "undefined"
            ? shop_data.app_url_css
            : "https://dashboard.handprint.tech";
    let handPrintHost =
        typeof shop_data.dashboard_api_url !== "undefined"
            ? shop_data.dashboard_api_url
            : "https://dashboard.handprint.tech/ext/api/v3";
    handPrintHost = handPrintHost.replace("v2", "v3");
    console.log("----- host", host);
    console.log("----- handPrintHost", handPrintHost);
    const apiEndPoint = host + "/api";

    var shopifyDomain = shop_data.shop.permanent_domain;
    //var cartData = shop_data.cart;

    var selectorHtml = $$$("main form[action='/cart']");
    var selectCartDrawer = $$$("form[action='/cart']:first-child");
    var moduleData = "";
    var checkCheckout = false;
    function buildHtml(widgetPosition, cartOption) {
        if (cartOption === "Cart Page") {
            let wPos = "center";
            if (["Top Left", "Bottom Left"].includes(widgetPosition))
                wPos = "left";
            else if (["Top Right", "Bottom Right"].includes(widgetPosition))
                wPos = "right";
            else if (["Top Center", "Bottom Center"].includes(widgetPosition))
                wPos = "center";
            return (
                `<link rel="stylesheet" href="https://dashboard.handprint.tech/themes/front/default/assets/css/widget-sp.css"/><div style= "float:` +
                wPos +
                `"  id="hp-iframe-div" class="hp-iframe-div"></div>`
            );
        }
        if (cartOption === "Cart Drawer") {
            return `<link rel="stylesheet" href="https://dashboard.handprint.tech/themes/front/default/assets/css/widget-sp.css" /><div style="width:100%" id="hp-iframe-div" class="hp-iframe-div"></div>`;
        }
    }

    $$$(document).ready(function() {
        const open = window.XMLHttpRequest.prototype.open;
        let isLoaded = false;

        function openReplacement() {
            this.addEventListener("load", function() {
                if (!isLoaded && $$$("form[action='/cart']")) {
                    // loadWidgetOnDynamicForm()
                }
                if (
                    [
                        "/cart/add.js",
                        // "/cart/update.js",
                        "/cart/change.js",
                        "/cart/clear.js"
                    ].includes(this._url)
                ) {
                    console.log("-------this._url", this._url);
                    prepareIframeModule(true);
                }
            });
            return open.apply(this, arguments);
        }
        window.XMLHttpRequest.prototype.open = openReplacement;

        function prepareIframeModule(hasFrameAlreadyLoaded = false) {
            let aPIEndPoint = apiEndPoint + "/" + shopifyDomain + "/widget";
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var data = JSON.parse(xhttp.responseText);
                    moduleData = data.data;
                    if (moduleData.status) {
                        const newHtmlRender = new XMLHttpRequest();
                        newHtmlRender.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                if (!data.data.settings.cart_position) {
                                    data.data.settings.cart_position =
                                        "Cart Page";
                                }
                                var newHtml = JSON.stringify(
                                    buildHtml(
                                        moduleData.settings.button_position,
                                        moduleData.settings.cart_position
                                    )
                                );

                                if (hasFrameAlreadyLoaded) {
                                    $$$("#hp-iframe-div").remove();
                                }
                                initSelector(
                                    data.data.settings.button_position,
                                    data.data.settings.cart_position,
                                    JSON.parse(newHtml)
                                );

                                var htmlResponse = JSON.stringify(
                                    newHtmlRender.responseText
                                );
                                var iframe = document.createElement("iframe");

                                var findIframeDiv = document.getElementById(
                                    "hp-iframe-div"
                                );
                                if (!findIframeDiv) return "";

                                findIframeDiv.appendChild(iframe);
                                iframe.src = "#";
                                iframe.id = "iframe-tag";
                                iframe.contentWindow.document.open();
                                htmlResponse = JSON.parse(htmlResponse);
                                iframe.contentWindow.document.write(
                                    htmlResponse
                                );
                                iframe.contentWindow.document.close();

                                if (
                                    moduleData.settings.cart_position ===
                                    "Cart Drawer"
                                ) {
                                    $$$("#iframe-tag").addClass(
                                        "z-index-sidedrawer"
                                    );
                                } else {
                                    $$$("#iframe-tag").addClass(
                                        "z-index-cartpage"
                                    );
                                }
                                document
                                    .getElementById("iframe-tag")
                                    .setAttribute("data-switch", "active");
                            }
                        };
                        setTimeout(async function() {
                            newHtmlRender.open(
                                "GET",
                                await callThirdParty(moduleData.api_user_id),
                                true
                            );

                            newHtmlRender.send();
                        }, 1000);
                    }
                }
            };
            xhttp.open("GET", aPIEndPoint, true);
            xhttp.send();
        }
        prepareIframeModule();

        $$$(document).on(
            "change",
            'input[name="updates[]"], input.cart-drawer__input',
            function() {
                console.log("moduleData");
                prepareIframeModule(true);
            }
        );
        if (shopifyDomain === "art-by-life.myshopify.com") {
            $$$(document).on(
                "click",
                ".js-qty__adjust--plus, .js-qty__adjust--minus, .add-to-cart",
                function() {
                    prepareIframeModule(true);
                }
            );
        }
    });

    async function callThirdParty(apiUserId) {
        const cartJson = await fetch("/cart.js")
            .then(response => response.json())
            .then(data => {
                return data;
            });
        const prepareApiRequest =
            handPrintHost +
            "/get-widget/" +
            apiUserId +
            "?quantity=" +
            cartJson.item_count +
            "&amount=" +
            (cartJson.items_subtotal_price / 100).toFixed(2) +
            "&currency_code=" +
            cartJson.currency +
            "&lang=" +
            document.getElementsByTagName("html")[0].getAttribute("lang");

        return prepareApiRequest;
    }

    window.addEventListener("resize", function() {
        resize_main_div();
    });

    function resize_main_div() {
        var width = document.querySelector(".handprint_zoom_saas");

        if (width !== null) {
            width = width.parentNode.parentNode.offsetWidth;

            if (width <= 530) {
                document.querySelector(".handprint_zoom_saas").style.zoom =
                    "0.46";
            } else {
                document.querySelector(".handprint_zoom_saas").style.zoom = "1";
            }
        }
    }

    function addCartNoteAttribute(htmlSelector, value) {
        if ($$$(".pledge-hidden").length === 0) {
            const cartAttr = `<input class="pledge-hidden" type="hidden" name="attributes[pledge]" value="${value}">`;
            htmlSelector.append(cartAttr);
        } else {
            $$$(".pledge-hidden").val(value);
        }

        $$$.ajax({
            url: "/cart/update.js",
            type: "POST",
            dataType: "jsonp",
            data: { "attributes[pledge]": `${value}` },
            success: function(result) {
                console.log("/cart/update pass");
            },
            error: function(jqxhr, status, exception) {
                console.log("/cart/update fail");
            }
        });
    }

    function initSelector(button_position, cart_position, newHtmlTemplate) {
        if (cart_position === "Cart Page") {
            if (
                $$$("#shopify-section-cart-template form[action='/cart']")
                    .length >= 1
            ) {
                selectorHtml = $$$(
                    "#shopify-section-cart-template form[action='/cart']"
                );
            } else if (
                $$$("form[action='/cart']").length >= 1 &&
                Shopify !== undefined &&
                Shopify.theme !== undefined &&
                Shopify.theme.name === "Hoolah-integration"
            ) {
                selectorHtml = $$$("form[action='/cart']");
            } else if (
                $$$("form[action='/cart']").length >= 1 &&
                Shopify !== undefined &&
                Shopify.theme !== undefined &&
                Shopify.theme.name === "Indytute + Clerk"
            ) {
                selectorHtml = $$$("form[action='/cart']");
            } else if (
                $$$(".main-content form[action='/cart']").length >= 1 &&
                Shopify !== undefined &&
                Shopify.theme !== undefined &&
                Shopify.theme.name === "DROP 2 NEW AGE"
            ) {
                selectorHtml = $$$(".main-content form[action='/cart']");
            } else if (
                $$$("section form[action='/cart']").length >= 1 &&
                shopifyDomain === "salientlabel.myshopify.com"
            ) {
                selectorHtml = $$$("section form[action='/cart']");
            } else if (shopifyDomain === "venque-craft-co-ca.myshopify.com") {
                selectorHtml = $$$("#cart_form");
            } else if (
                $$$("section form[action='/cart']").length >= 1 &&
                shopifyDomain === "tgo-singapore.myshopify.com"
            ) {
                selectorHtml = $$$("section form[action='/cart']");
            } else if (
                $$$(".template-cart #MainContent .shopify-section").length >=
                    1 &&
                shopifyDomain === "widefitshoes-uk.myshopify.com"
            ) {
                selectorHtml = $$$(
                    ".template-cart #MainContent .shopify-section"
                );
            } else if (shopifyDomain === "uk-cambridgesatchel.myshopify.com") {
                selectorHtml = $$$("main .klevuTarget");
            } else if (shopifyDomain === "tamlifestyle.myshopify.com") {
                selectorHtml = $$$("#shopify-section-footer");
            } else if (
                shopifyDomain === "sulinashop-com.myshopify.com" &&
                $$$(
                    "#MainContent .cart__footer__additional .additional-checkout-buttons"
                ).length >= 1
            ) {
                selectorHtml = $$$(
                    "#MainContent .cart__footer__additional .additional-checkout-buttons"
                );
            } else if ($$$("main form[action='/cart']").length >= 1) {
                selectorHtml = $$$("main form[action='/cart']");
            }
            setWidgetPosition(selectorHtml, newHtmlTemplate, button_position);
        }
        if (cart_position === "Cart Drawer") {
            if (shopifyDomain === "g-o-t-b.myshopify.com") {
                selectCartDrawer = $$$(
                    "form[action='/cart'] > .ajaxcart__inner.ajaxcart__inner--has-fixed-footer > .ajaxcart__product:last-child"
                );
            } else if (shopifyDomain === "zerrin.myshopify.com") {
                // selectCartDrawer = $$$(".drawer__inner.drawer__inner--has-fixed-footer > .appear-animation:last-child");
                selectCartDrawer = $$$(
                    ".drawer__inner > .appear-animation:last-child"
                );
            } else if (
                shopifyDomain === "mutt-motorcycles-singapore.myshopify.com"
            ) {
                selectCartDrawer = $$$(
                    ".ajaxcart__inner.ajaxcart__inner--has-fixed-footer > .ajaxcart__row:last-child"
                );
            } else if (shopifyDomain === "saltybottom.myshopify.com") {
                selectCartDrawer = $$$(
                    ".Cart.Drawer__Content > .Drawer__Footer"
                );
            } else if (shopifyDomain === "widefitshoes-uk.myshopify.com") {
                selectCartDrawer = $$$("#side_cart .mini_cart .footer");
            } else if (
                shopifyDomain === "inezirae-schmuckdesign.myshopify.com"
            ) {
                $$$(".Cart.Drawer__Content > .Drawer__Footer").css(
                    "overflow",
                    "auto"
                );
                selectCartDrawer = $$$(
                    ".Cart.Drawer__Content > .Drawer__Footer"
                );
            } else if ($$$(".cart-widget-side").length >= 1) {
                selectCartDrawer = $$$(".cart-widget-side");
            } else if (selectCartDrawer.length === 0) {
                if ($$$("form[action='/cart']").length) {
                    selectCartDrawer = $$$("form[action='/cart']").first();
                }
            }

            setWidgetPositionCartDrawer(
                selectCartDrawer,
                newHtmlTemplate,
                button_position
            );
        }
    }

    function setWidgetPosition(
        selectorHtml,
        handprintLoadApp,
        button_position
    ) {
        switch (button_position) {
            case "Bottom":
            case "Bottom Left":
            case "Bottom Center":
            case "Bottom Right":
                selectorHtml.after(handprintLoadApp);
                break;
            case "Top":
            case "Top Left":
            case "Top Center":
            case "Top Right":
                selectorHtml.before(handprintLoadApp);
                break;
        }
    }

    function setWidgetPositionCartDrawer(
        selectorHtml,
        handprintLoadApp,
        button_position
    ) {
        switch (button_position) {
            case "Bottom":
            case "Bottom Left":
            case "Bottom Center":
            case "Bottom Right":
                selectorHtml.append(handprintLoadApp);
                resize_main_div();
                break;
            case "Top":
            case "Top Left":
            case "Top Center":
            case "Top Right":
                selectorHtml.prepend(handprintLoadApp);
                resize_main_div();
                break;
        }
    }
}
