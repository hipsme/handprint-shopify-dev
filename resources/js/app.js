require('./bootstrap');

window.Vue = require('vue');

import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.use(Loading);


import VueToast from 'vue-toast-notification';
// Import one of available themes
import 'vue-toast-notification/dist/theme-default.css';
//import 'vue-toast-notification/dist/theme-sugar.css';
import { ValidationProvider, ValidationObserver } from 'vee-validate';

Vue.use(VueToast,{
    position: 'bottom'
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('setting-index-component', require('./components/setting/Master').default);
Vue.component('orders-component', require('./components/orders/Master').default);
Vue.component('widget', require('./components/widget/Master').default);
Vue.component("model", require('./components/shopify/Model').default);

import helpers from './helpers/mixins.js';
const plugin = {
    install () {
        Vue.prototype.$helpers = helpers
    }
}

Vue.use(plugin);

const app = new Vue({
    el: '#app',
});
