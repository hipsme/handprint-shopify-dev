import {Toast, Loading, Modal, Button} from '@shopify/app-bridge/actions';
var loading = '';
export default {
    initLoading(){
        loading = Loading.create(window.shopify_app_bridge);
    },
    startLoading(){
        if(window.shopify_app_bridge != undefined){
            this.initLoading();
            loading.dispatch(Loading.Action.START);
        }
    },
    stopLoading(){
        if(window.shopify_app_bridge != undefined){
            this.initLoading();
            loading.dispatch(Loading.Action.STOP);
        }
    },
    successToast(message){
        if(window.shopify_app_bridge != undefined){
            let toastNotice = Toast.create(window.shopify_app_bridge, {message:message,duration: 2000});
            toastNotice.dispatch(Toast.Action.SHOW);
        } else {
            Vue.$toast.default(message, {duration: 2000});
        }
    },
    errorToast(message){
        if(window.shopify_app_bridge != undefined){
            let toastNotice = Toast.create(window.shopify_app_bridge, {message:message,duration: 2000,isError: true});
        toastNotice.dispatch(Toast.Action.SHOW);
        } else {
            Vue.$toast.error(message, {duration: 2000});
        }
    },
    confirmModel(){

        const okButton = Button.create(window.shopify_app_bridge, {label: 'Ok'});
        okButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok okButton');
        });
        const cancelButton = Button.create(window.shopify_app_bridge, {label: 'Cancel'});
        cancelButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok cancelButton');
        });
        const modalOptions = {
            title: 'My Modal',
            footer: {
                buttons: {
                    primary: okButton,
                    secondary: [cancelButton],
                },
            },
        };

        const myModal = Modal.create(window.shopify_app_bridge, modalOptions);

        myModal.subscribe(Modal.Action.OPEN, () => {
            console.log('Ok OPEN');
        });

        myModal.subscribe(Modal.Action.CLOSE, () => {
            console.log('Ok CLOSE');
        });
    }
};
