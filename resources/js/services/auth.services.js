import AxiosService from './axios.services';

const postUrl = process.env.HANDPRINT_API_URL + '/ext/api/';

class AuthService {
    getToken() {
        let data = new FormData();
        data.append('key', '3aef8d0e-4ad7-4c4f-97a2-12fdd0e2454c');
        data.append('password', 'DSCUklgq$@*5AKpZV%S^Wy4');

        AxiosService
            .post(postUrl + 'v1/login', data)
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    authHeader() {
        let user = JSON.parse(localStorage.getItem('user'));
        if (user && user.token) {
            return { Authorization: 'Bearer ' + user.token };
        } else {
            return null;
        }
    }
}

export default new AuthService();
