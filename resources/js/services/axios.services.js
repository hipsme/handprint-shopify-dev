import axios from "axios";
import AuthService from  './auth.services'

const postUrl = process.env.HANDPRINT_API_URL + '/ext/api/';

class AxiosService {
    get(url) {
        return this.responseHandler(
            axios.get(url, { headers: AuthService.authHeader() })
        );
        // return axios.get(postUrl + 'v2/get-widget/24a57397-5df7-4465-b8e7-d2f59dc13a7f')
    }

    post(url, body) {
        return this.responseHandler(
            axios.post(url, body, { headers: AuthService.authHeader()})
        );
    }

    put(url, body ) {
        return this.responseHandler(axios.put(url, body));
    }

    delete(url) {
        return this.responseHandler(axios.delete(url));
    }

    async responseHandler(responsePromise) {
        try {
            const response = await responsePromise;
            return response && response.data;
        } catch (error) {
            console.log("error------", error);
            if (error && error.response && error.response.data && error.response.data) {

            }
            throw error;
        }
    }
}

export default new AxiosService();
