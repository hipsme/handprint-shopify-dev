## create .env file in project root directory and paste the contents for `handprint_env` this file is provided in skype

### Commands, we have to run on server, for setup project
```
1. composer install
2. php artisan migrate
3. npm install
4. npm run production
```


### Install the supervisor in the server and create the supervisor conf file and paste the below contents

```
[program:handprint-queue-worker]
#process_name=%(program_name)s_%(process_num)02d
command=php /var/www/html/handprint/artisan queue:work --sleep=3 --tries=1
autostart=true
autorestart=true
#user=www-data
#numprocs=8
#redirect_stderr=true
```
Then
```
1) sudo supervisorctl reread
output: handprint-queue-worker: available

2)sudo supervisorctl update
output: handprint-queue-worker: added process group

3)sudo supervisorctl restart all
output: handprint-queue-worker: stopped
	handprint-queue-worker: started
```

