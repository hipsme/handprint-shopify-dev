const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
// const config = require('./webpack.config')
// mix.webpackConfig(config)
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/app-label.js', 'public/js/app-label.js')
    .postCss('resources/css/app.css', 'public/css', []);

mix.postCss('resources/css/admin.css', 'public/css/admin.css');
mix.postCss('resources/css/front.css', 'public/css/front.css');

mix.version();
