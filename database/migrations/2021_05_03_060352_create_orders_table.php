<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shopify_order_id');
            $table->string('customer_id')->nullable(true);
            $table->string('customer_name')->nullable(true);
            $table->string('customer_email')->nullable(true);
            $table->string('customer_country')->nullable(true);
            $table->string('customer_state')->nullable(true);
            $table->string('customer_zipcode')->nullable(true);
            $table->string('order_note')->nullable(true);
            $table->dateTime('order_created_at');
            $table->longText('order_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
