<?php

use App\Http\Controllers\Api\Widget\WidgetController;
use App\Http\Controllers\Api\Order\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::group(['prefix' => '{shopifyShop}'], function () {
        Route::get("/widget", [WidgetController::class, 'index']);
    });
    Route::get("orders", [OrderController::class, 'index'])->name('api.orders');
});

Route::get('/health_check', function () {
    return response('health_check', 200)
                  ->header('Content-Type', 'text/plain');
});
