<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TestWebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// dd(\Request::server());

Route::group(["middleware" => ["web"]], function() {

    Route::group(["middleware" => ["auth.shopify"]], function () {

        /* Setting Controller */
        Route::get("/", [SettingController::class, 'index'])->name('home');

        Route::post("/store", [SettingController::class, 'store'])->name('store');
        Route::post("/status", [SettingController::class, 'status'])->name('status');

        /* Plan & Pricing */
        Route::view("plan-pricing","pages.plan-pricing.index")->name('plan-pricing');

        /* Installation Guide */
        Route::view("installation-guide","pages.installation-guide.index")->name('installation-guide');

        /* Orders listing */

        Route::view("orders","pages.orders.index")->name('orders');
    });
});

Route::get('flush', function(){
    request()->session()->flush();
});

Route::get('test', [TestController::class, 'index']);
Route::get('login-api', [TestController::class, 'loginApi']);
Route::get('storefront-widget', [TestController::class, 'storeFront']);
Route::get('add-snippet', [TestController::class, 'addSnippetFromBrowser']);
Route::get('find-missing-orders', [TestController::class, 'findMissingOrders']);

Route::get('/webhooks/', [TestWebhookController::class, 'get']);

//Route::view('health_check', 'aws.health_check');

Route::get('/health_check', function () {
    return response('health_check', 200)
                  ->header('Content-Type', 'text/plain');
});
