packages:
  python:
    supervisor: []
container_commands:
    00_linking_supervisord:
        command: "ln -s /usr/bin/supervisord /usr/local/bin/supervisord --force"
    01_start_supervisor:
        command: "systemctl restart supervisord.service"    
    02_reload_supervisor:
        command: "systemctl reload supervisord.service"
    03_start_supervisor:
        command: "systemctl restart supervisord.service"
    04_create_post_dir:
        command: "mkdir /opt/elasticbeanstalk/hooks/appdeploy/post"
        ignoreErrors: true
files:
    "/opt/elasticbeanstalk/hooks/appdeploy/post/job_after_deploy.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/usr/bin/env bash
            # to terminate horizon process of the old version (supervisor will start the new one for us <3 )
            #source /opt/elasticbeanstalk/support/envvars
            (cd /var/app/current && laravel-echo-server stop)
    "/var/log/supervisor/supervisord.log":
        mode: "000644"
        owner: root
        group: root
        content: |
            start of the log file
    "/horizon-script.sh":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/sh
            echo "Starting horizon.sh"
            #. /opt/elasticbeanstalk/support/envvars
            (cd /var/app/current && laravel-echo-server start)            
    
    "/etc/supervisor/conf.d/horizon-worker.conf":
        mode: "000755"
        owner: root
        group: root
        content: |
            [program:horizon-worker]
            process_name=%(program_name)s_%(process_num)02d
            command=php /var/app/current/artisan queue:work --sleep=3 --tries=1
            autostart=true
            autorestart=true
            user=root
            redirect_stderr=true
            stdout_logfile=/var/log/horizon-worker.log
    "/usr/local/etc/supervisord.conf":
        mode: "000644"
        owner: root
        group: root
        content: |
            ; supervisor config file
            [unix_http_server]
            file=/var/run/supervisor.sock   ; (the path to the socket file)
            chmod=0700                       ; sockef file mode (default 0700)
            [supervisord]
            logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
            pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
            childlogdir=/var/log/supervisor            ; ('AUTO' child log dir, default $TEMP)
            ; the below section must remain in the config file for RPC
            ; (supervisorctl/web interface) to work, additional interfaces may be
            ; added by defining them in separate rpcinterface: sections
            [rpcinterface:supervisor]
            supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
            [supervisorctl]
            serverurl=unix:///var/run/supervisor.sock ; use a unix:// URL  for a unix socket
            ; The [include] section can just contain the "files" setting.  This
            ; setting can list multiple files (separated by whitespace or
            ; newlines).  It can also contain wildcards.  The filenames are
            ; interpreted as relative to this file.  Included files *cannot*
            ; include files themselves.
            [include]
            files = /etc/supervisor/conf.d/*.conf
            ; Change according to your configurations
    
    "/etc/init.d/supervisord":
        mode: "000755"
        owner: root
        group: root
        content: |
            #!/bin/bash
            # Source function library
            . /etc/rc.d/init.d/functions
            # Source system settings
            if [ -f /etc/sysconfig/supervisord ]; then
                . /etc/sysconfig/supervisord
            fi
            # Path to the supervisorctl script, server binary,
            # and short-form for messages.
            supervisorctl=/usr/local/bin/supervisorctl
            supervisord=${SUPERVISORD-/usr/local/bin/supervisord}
            prog=supervisord
            pidfile=${PIDFILE-/var/run/supervisord.pid}
            lockfile=${LOCKFILE-/var/lock/subsys/supervisord}
            STOP_TIMEOUT=${STOP_TIMEOUT-60}
            OPTIONS="${OPTIONS--c /usr/local/etc/supervisord.conf}"
            RETVAL=0
            start() {
                echo -n $"Starting $prog: "
                daemon --pidfile=${pidfile} $supervisord $OPTIONS
                RETVAL=$?
                echo
                if [ $RETVAL -eq 0 ]; then
                    touch ${lockfile}
                    $supervisorctl $OPTIONS status
                fi
                return $RETVAL
            }
            stop() {
                echo -n $"Stopping $prog: "
                killproc -p ${pidfile} -d ${STOP_TIMEOUT} $supervisord
                RETVAL=$?
                echo
                [ $RETVAL -eq 0 ] && rm -rf ${lockfile} ${pidfile}
            }
            reload() {
                echo -n $"Reloading $prog: "
                LSB=1 killproc -p $pidfile $supervisord -HUP
                RETVAL=$?
                echo
                if [ $RETVAL -eq 7 ]; then
                    failure $"$prog reload"
                else
                    $supervisorctl $OPTIONS status
                fi
            }
            restart() {
                stop
                start
            }
            case "$1" in
                start)
                    start
                    ;;
                stop)
                    stop
                    ;;
                status)
                    status -p ${pidfile} $supervisord
                    RETVAL=$?
                    [ $RETVAL -eq 0 ] && $supervisorctl $OPTIONS status
                    ;;
                restart)
                    restart
                    ;;
                condrestart|try-restart)
                    if status -p ${pidfile} $supervisord >&/dev/null; then
                    stop
                    start
                    fi
                    ;;
                force-reload|reload)
                    reload
                    ;;
                *)
                    echo $"Usage: $prog {start|stop|restart|condrestart|try-restart|force-reload|reload}"
                    RETVAL=2
                esac
                exit $RETVAL
