<?php

use Illuminate\Support\Facades\Http;

function nullToEmpty($string, $emptyString = '')
{
    return !is_null($string) ? $string : $emptyString;
}

function adminNav(){
    /*
     |--------------------------------------------------------------------------
     | Admin Navigation Menu
     |--------------------------------------------------------------------------
     |
     | This array is for Navigation menus of the backend.  Just add/edit or
     | remove the elements from this array which will automatically change the
     | navigation.
     |
     */
    // SIDEBAR LAYOUT - MENU
    $menus_all = [
        [
            'title'  => 'Settings',
            'link'   => route('home'),
            'active' => (Request::path() == '/')?'Polaris-Tabs__Tab--selected':'',
        ],
        [
            'title'  => 'Plan & Pricing',
            'link'   => route('plan-pricing'),
            'active' => (Request::is('plan-pricing'))?'Polaris-Tabs__Tab--selected':'',
        ],
        [
            'title'  => 'User Guide',
            'link'   => route('installation-guide'),
            'active' => (Request::is('installation-guide'))?'Polaris-Tabs__Tab--selected':'',
        ],
        [
            'title'  => 'Orders',
            'link'   => route('orders'),
            'active' => (Request::is('orders'))?'Polaris-Tabs__Tab--selected':'',
        ],
    ];

    return $menus_all;
}


function loginApi() {
    $form = ["key" => config('app.handprint_login_key'), "password" => config('app.handprint_login_password')];
    $response = Http::asForm()->withHeaders([
        'Content-Type' => 'multipart/form-data',
    ])->post(config('app.handprint_api_url')."/login", $form);

    if($response->status()) {
        return json_decode($response->body(),1)['data'];
    }
    return false;
}

function statusChanged($status, $user) {
    $url = replaceApiUrlVersion("v4");
    $input["key"] = $user->api_user_id;
    $input["status"] = $status;
    //$input["store_name"] = $user->name;
    $input["store_url"] = "https://".$user->name;
    //$input["email"] = "";
    //$input["place"] = @$user->setting->visual_guide['cart_position'];
    $response  = Http::asForm()->withHeaders([
        'Content-Type' => 'multipart/form-data',
    ])->post($url.'/save-status', $input);

    if($response->status()) {
        return json_decode($response->body(),1);
    }
}

function replaceApiUrlVersion($version){
    $url = config('app.handprint_api_url');
    $url = str_replace("v1", $version, $url);
    $url = str_replace("v2", $version, $url);
    $url = str_replace("v3", $version, $url);
    $url = str_replace("v4", $version, $url);
    return $url;
}
