<?php namespace App\Jobs;

use App\Models\User;
use App\Models\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        // Convert domain
        // Log::info('called OrdersCreateJob');

        $shopEmail = $this->shopDomain;
        /** @var User $shop */
        $shop = User::where('name', $shopEmail)->first();

        $order = json_encode($this->data);
        // Log::info("Orders----data");
        // Log::info($order);
        // Log::info("Orders----data");
        $shopify_id = json_decode($order)->id;

        $entity = Webhook::updateOrCreate(
            ['shopify_id' => $shopify_id, 'topic' => 'orders/create', 'user_id' => $shop->id],
            ['shopify_id' => $shopify_id, 'topic' => 'orders/create', 'user_id' => $shop->id, 'data' => $order]
        );

        if (!$shop || empty($shop->api_user_id)) {
            // Log::info('Api user id not exist');
            return true;
        }

        ExecuteWebhookJob::dispatch($entity->id);
        return true;
    }
}
