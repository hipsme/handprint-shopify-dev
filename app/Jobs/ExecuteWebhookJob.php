<?php

namespace App\Jobs;

use App\Models\Order;
use App\Models\User;
use App\Models\Webhook;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExecuteWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhook_id;

    /**
     * Create a new job instance.
     *
     * @param string $webhookIid
     */
    public function __construct($webhookIid)
    {
        $this->webhook_id = $webhookIid;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        /** @var Webhook $webhook */
        $webhook = Webhook::where('is_executed',0)->find($this->webhook_id);
        if (empty($webhook)) {
            return true;
        }

        try {
            $shop = User::where('id', $webhook->user_id)->firstOrFail();

            $data = json_decode($webhook->data,1);

            $cartTotal = $data['total_price'];
            $itemQty = array_sum(Arr::pluck($data['line_items'], 'quantity'));

            $pledgeValue = "de-active";
            $note_attr = $data['note_attributes'];

              /* Log::info("--- START Domain: ". $shop->name);
              Log::info("--- pledgeValue: ". $pledgeValue);
              Log::info("--- note_attributes");
              Log::info(json_encode($note_attr)); */
//            Log::info("--------Data--------");
//            Log::info($data);
//            Log::info("--------End Data--------");

            /* Log::info("----------------");
            Log::info($note_attr);
            Log::info("----------------"); */
            $pos = isset($data['client_details']["user_agent"])?$data['client_details']["user_agent"]:NULL;
            if(count($note_attr) >= 1){
                foreach ($note_attr as $attribute){
                    //Log::info("------INPUT attribute Values----------");
                    if($attribute['name'] === "pledge")
                        $pledgeValue = $attribute['value'];
//                    Log::info("------INPUT pledgeValue Values----------");
//                    Log::info($pledgeValue);
                }
            }else if(strpos($pos, 'POS') !== false){ // set this for POS
                $pledgeValue = "active";
                $pos = 1;
            }


            $input = [
                'key' => @$shop->api_user_id,
                'be' => @$data['email'],
                'on' => $data['id'],
                'don' => $data['order_number'],
                'qty' => @$itemQty,
                'pa' =>  @$cartTotal,
                'currency' => @$data['currency'],
                'pl' => in_array($pledgeValue,['active', 'packaging-free']) ? 1 : 0,
                'pf' => 'sho'
            ];
            //Log::info("------INPUT Values----------");

            /* Log::info("input array------- ", $input);
            Log::info("--- after pledgeValue: ". $pledgeValue); */
            if(in_array($pledgeValue,['active', 'packaging-free'])){
                //Log::info("------ IN API CALL");
                Http::asForm()->withHeaders([
                    'Content-Type' => 'multipart/form-data',
                ])->post(config('app.handprint_api_url').'/save-order', $input);
            }

            $this->saveOrders($shop, $data, $pledgeValue, $pos);

            //Log::info('Called Post API');

            $webhook->is_executed = 1;
            $webhook->save();
            //Log::info("------save-order API successfully called END----------");
            return true;
        } catch (\Exception $exception) {
            /* Log::error("Save Order API Failed");
            Log::error($exception->getMessage()); */
        }
    }

    public function saveOrders($shop, $data, $pledgeValue, $pos){

//         Log::info("------saveOrders function----------");
//         Log::info(@$data['created_at']);
//         Log::info(Carbon::parse(@$data['created_at'])->format('Y-m-d H:i:s'));

        $order = new Order();
        $order->user_id = $shop->id;
        $order->shopify_order_id = @$data['id'];
        $order->shopify_display_order_id = $data['order_number'];
        $order->customer_id = @$data['customer']['id'];
        $order->customer_name = @$data['customer']['first_name']." ". @$data['customer']['last_name'];
        $order->customer_email = @$data['customer']['email'];
        $order->customer_country = @$data['customer']['default_address']['country'];
        $order->customer_state = @$data['customer']['default_address']['province'];
        $order->customer_zipcode = @$data['customer']['default_address']['zip'];
        $order->order_note = $pledgeValue;
        $order->order_created_at = Carbon::parse(@$data['created_at'])->format('Y-m-d H:i:s'); // $shop->created_at;
        $order->order_details = json_encode($data);
        $order->is_pos = $pos === 1? 1 :0;
        $order->save();
        //Log::info("------Save Orders function completed----------");
    }
}
