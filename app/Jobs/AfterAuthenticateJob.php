<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->snippet();
    }

    public function snippet()
    {
        $app_url_css = $app_url = config('app.url');
        if(config('app.url') == "https://staging.shop.handprint.tech"){
            $app_url_css = "https://staging.handprint.tech";
        }else if(config('app.url') == "https://shop.handprint.tech"){
            $app_url_css = "https://dashboard.handprint.tech";
        }
        $dashboard_api_url = config('app.handprint_api_url');
        $shop = \Auth::user();
        $value = <<<EOF
        <script id="panther_label_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }},
                "app_url": "$app_url",
                "app_url_css": "$app_url_css",
                "dashboard_api_url": "$dashboard_api_url"
            }
        </script>
EOF;
        \Log::info($shop->name);
        $sh_theme = $shop->api()->rest('GET', '/admin/themes.json',['role' => 'main']);

        $main_theme = $sh_theme['body']->container['themes'][0]['id'];

        $parameter['asset']['key'] = 'snippets/panther-label.liquid';
        $parameter['asset']['value'] = $value;

        // add new liquid file ( panther-label )
        $asset = $shop->api()->rest('PUT', '/admin/themes/'.$main_theme.'/assets.json',$parameter);

        // include that liquid file in theme file
        $asset = $shop->api()->rest('GET', '/admin/themes/'.$main_theme.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
        if(@$asset['body']->container['asset']) {
            $asset = $asset['body']->container['asset']['value'];

            $shop->theme_content = $asset;
            $shop->status = 1;
            $shop->save();

            // check if already included
            if(!strpos($asset ,"{% include 'panther-label' %}</head>")) {
                $asset = str_replace('</head>',"{% include 'panther-label' %}</head> ",$asset);

                $parameter = [];
                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $asset = $shop->api()->rest('PUT', '/admin/themes/'.$main_theme.'/assets.json',$parameter);
            }
            statusChanged("install", $shop);
        }
    }
}
