<?php

namespace App\Jobs;

use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    public function handle(
        IShopCommand $shopCommand,
        IShopQuery $shopQuery,
        CancelCurrentPlan $cancelCurrentPlanAction
    ): bool {

        \Log::info("Uninstall webhook called.");
        // Convert the domain
        $this->domain = ShopDomain::fromNative($this->domain);

        // Get the shop
        $shop = $shopQuery->getByDomain($this->domain);
        $shopId = $shop->getId();
        statusChanged("uninstall", $shop);
        // Cancel the current plan
        $cancelCurrentPlanAction($shopId);
        // Purge shop of token, plan, etc.
        $shopCommand->clean($shopId);
        // Soft delete the shop.
        $shopCommand->softDelete($shopId);
        $shop->newly = 0;
        $shop->status = 0;
        $shop->save();

        return true;
    }
}
