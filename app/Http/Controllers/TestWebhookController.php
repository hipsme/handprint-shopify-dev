<?php
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Webhook;
use Illuminate\Http\Request;

class TestWebhookController extends Controller {
    public function get(Request $request)
    {
        $shop = User::where("name", $request->shop)->first();
        $data = $shop->api()->rest('GET', '/admin/webhooks.json');
        dd($data);
    }
    public function destroy(Request $request)
    {
        $shop = \Auth::user();
        $data = $shop->api()->rest("DELETE", '/admin/webhooks/'.$request->id.'.json');
        dd($data);
    }
    public function syncWebhookProduct(Request $request){
        $webhooks = Webhook::where('topic', 'orders/updated')->toBase()->get();
        foreach ($webhooks as $key => $val){
            SyncOrderJob::dispatch($val->id);
        }
        //SyncOrderJob::dispatch($request->webhook_id);
    }
}
