<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|string
     */
    public function index(Request $request)
    {
        $user = \Auth::user();

        if($user->newly == 0){
            $user->newly = 1;
            $user->save();

            // $app_url = $app_url = config('app.url');
            // if(config('app.url') == "https://staging.shop.handprint.tech" || config('app.url') == "https://shopify-plugin.test"){
            //     $app_url = "https://staging.handprint.tech";
            // }else if(config('app.url') == "https://shop.handprint.tech"){
            //     $app_url = "https://dashboard.handprint.tech";
            // }
            // return redirect($app_url . '/set-user-login/'.$user->api_user_id);

            return view('congratulations', compact("user"));
        }

        if ($request->filled('api')) {
            return $this->apiIndex($request);
        }
        if(!empty($request->serial_key) && $request->serial_key != "empty") {

            $user->api_user_id = @$request->serial_key;
            $user->save();
            return redirect()->route('home');
        }

        return view('pages.setting.index');
    }

    /**
     * @param $request
     * @return array|string
     */
    public function apiIndex($request)
    {
        $user = \Auth::user();

        $loginData = [];
        $responseData = [
            'id' => $user->id,
            'api_user_id' => $user->api_user_id,
            'api_login_token' => $user->api_login_token,
            'settings' => @$user->setting->visual_guide,
            'handprint_api_url' => config('app.handprint_api_url'),
            'login_data' => $loginData
        ];

        return $responseData;
    }

    /**
 * Store a newly created resource in storage.
 *
 * @param Request $request
 * @return \App\Http\Controllers\JsonResponse|\Illuminate\Http\JsonResponse
 */
    public function store(Request $request)
    {
        $user = Auth::user();
        $user->api_user_id = @$request->api_user_id;
        $user->save();
        $this->saveSetting($request);
        return $this->sendSuccess('Record Successfully Saved!');
    }

    public function status(Request $request)
    {
        $user = Auth::user();
        $user->status = @$request->status;
        $user->save();
        $status = ($request->status == 0)?"inactive":"active";
        statusChanged($status, $user);
        return $this->sendSuccess('Status Successfully Changed');
    }

    public function saveSetting(Request $request)
    {
        $user = Auth::user();
        Setting::updateOrCreate(
            [ 'user_id' => $user->id ],
            [ 'user_id' => $user->id, 'visual_guide' =>  json_decode($request->visual_guide,1)]
        );
        return true;
    }
}
