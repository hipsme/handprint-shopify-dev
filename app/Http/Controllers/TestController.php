<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use App\Models\Webhook;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;


class TestController extends Controller
{

    public function index()
    {
        /** @var Webhook $webhook */
        $webhook = Webhook::where('is_executed',0)->find(1);
        if (empty($webhook)) {
            return true;
        }

        $shop = User::where('id', $webhook->user_id)->firstOrFail();

        $data = json_decode($webhook->data,1);

        $response = Http::get(config('app.handprint_api_url').'get-widget/'.$shop->api_user_id);
        $jsonData = $response->json();
        $apiData = $jsonData['data'];

        if ($apiData['mode'] !== 'cart.php') {
            Log::info('Not a cart page mode');
            return true;
        }

        $shortCode = $apiData['short_code'];
        $cartTotal = 152;
        $itemQty = 15;
        $treesAdded = 0;

        if (isset($apiData['project_id'])) {
            if ($shortCode == 'revenue') {
                $pledgeTotal = @$apiData['pledge']['total'];
                $spend = round($cartTotal * $pledgeTotal / 100);
                $treesAdded = round($spend / $apiData['unit_price'], 3);
            } elseif ($shortCode == 'carbon') {
                $spend = round(@$apiData['pledge']['average'] / $apiData['coefficient'] * $apiData['unit_price'], 3);
                $treesAdded = round($spend / $apiData['unit_price'], 3);
            }
        }

        $input = [
            'pi' => @$apiData['project_id'],
            'ci' => @$apiData['company_id'],
            'wi' => @$apiData['id'],
            'be' => @$data['email'],
            'qty' => @$itemQty,
            'pa' =>  @$cartTotal,
            'pda' => @$apiData['pledge']['total'],
            'currency' => @$data['currency'],
            'pl' => 1,
            'tree' => @$treesAdded,
        ];
        dd($input);
    }

    public function loginApi(){
        $loginData = loginApi();
        if($loginData !== false)
            return 1;
        else
            return 0;
    }

    public function storeFront(){
        return view("welcome");
    }

    public function addSnippetFromBrowser(Request $request){

        $shopDomain = @$request->get('shop');

        if (isset($shopDomain) || $request->shop=="all"){

            $users = User::when($request->shop!="all", function($q) use($shopDomain){
                $q->where('name', $shopDomain);
            })->whereNotNull('password')->get();
            foreach($users as $findUser){
                if (isset($findUser)){
                    $shop = Auth::loginUsingId($findUser->id); // Login using user id
                    $app_url_css = $app_url = config('app.url');
                    if(config('app.url') == "https://staging.shop.handprint.tech"){
                        $app_url_css = "https://staging.handprint.tech";
                    }else if(config('app.url') == "https://shop.handprint.tech"){
                        $app_url_css = "https://dashboard.handprint.tech";
                    }
                    $dashboard_api_url = config('app.handprint_api_url');

                    $value = <<<EOF
            <script id="panther_label_data" type="application/json">
                {
                    "shop": {
                        "domain": "{{ shop.domain }}",
                        "permanent_domain": "{{ shop.permanent_domain }}",
                        "url": "{{ shop.url }}",
                        "secure_url": "{{ shop.secure_url }}",
                        "money_format": {{ shop.money_format | json }},
                        "currency": {{ shop.currency | json }}
                    },
                    "customer": {
                        "id": {{ customer.id | json }},
                        "tags": {{ customer.tags | json }}
                    },
                    "cart": {{ cart | json }},
                    "template": "{{ template | split: "." | first }}",
                    "product": {{ product | json }},
                    "collection": {{ collection.products | json }},
                    "app_url": "$app_url",
                    "app_url_css": "$app_url_css",
                    "dashboard_api_url": "$dashboard_api_url"
                }
            </script>
    EOF;
                    \Log::info($shop->name);
                    $sh_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);

                    $main_theme = $sh_theme['body']->container['themes'][0]['id'];

                    $parameter['asset']['key'] = 'snippets/panther-label.liquid';
                    $parameter['asset']['value'] = $value;

                    //check if panther-label file already exist
                    /*$isFileExist = $shop->api()->rest('GET', 'admin/themes/'.$main_theme.'/assets.json', $parameter);
                    if ($isFileExist['errors'] === false) {
                        return "Error :- contents found!";
                    }*/

                    // add new liquid file ( panther-label )
                    $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);

                    // include that liquid file in theme file
                    $asset = $shop->api()->rest('GET', 'admin/themes/'.$main_theme.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
                    if(@$asset['body']->container['asset']) {
                        $asset = $asset['body']->container['asset']['value'];

                        $shop->theme_content = $asset;
                        $shop->save();

                        // check if already included
                        if(!strpos($asset ,"{% include 'panther-label' %}")) {
                            $asset = str_replace('</head>',"{% include 'panther-label' %}</head> ",$asset);

                            $parameter = [];
                            $parameter['asset']['key'] = 'layout/theme.liquid';
                            $parameter['asset']['value'] = $asset;
                            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);
                            echo "added {% include 'panther-label' %} in theme.js <br/>";
                        }else{
                            echo "{% include 'panther-label' %} already in theme.js <br/>";
                        }
                    }
                    dump("Success :- contents added!", $shop->name);
                }
            }


        }

    }

    public function findMissingOrders(Request $request) {
        // dump("29--" , Order::where('order_note', 'active')->whereBetween('created_at',['2021-12-29 00:00:00.000000', '2021-12-29 23:59:59.000000'])->count());
        // dump("30--" , Order::where('order_note', 'active')->whereBetween('created_at',['2021-12-30 00:00:00.000000', '2021-12-30 23:59:59.000000'])->count());
        // dump("31--" , Order::where('order_note', 'active')->whereBetween('created_at',['2021-12-31 00:00:00.000000', '2021-12-31 23:59:59.000000'])->count());
        // dump("01--" , Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-01 00:00:00.000000', '2022-01-01 23:59:59.000000'])->count());
        // dump("02--" , Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-02 00:00:00.000000', '2022-01-02 23:59:59.000000'])->count());
        // dump("03--" , Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-03 00:00:00.000000', '2022-01-03 23:59:59.000000'])->count());
        // dump("04--" , Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-04 00:00:00.000000', '2022-01-04 23:59:59.000000'])->count());

        // dd(1);
        // 29


        $inputed_date = $request->input('date');

        $start_time = Carbon::parse($inputed_date)->format('Y-m-d 00:00:00');
        $end_time = Carbon::parse($inputed_date)->format('Y-m-d 23:59:59');

        $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',[$start_time, $end_time])->get();

        // // 30
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2021-12-30 00:00:00.000000', '2021-12-30 23:59:59.000000'])->get();
        // // 31
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2021-12-31 00:00:00.000000', '2021-12-31 23:59:59.000000'])->get();
        // // 1
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-01 00:00:00.000000', '2022-01-01 23:59:59.000000'])->get();
        // // 2
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-02 00:00:00.000000', '2022-01-02 23:59:59.000000'])->get();
        // // 3
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-03 00:00:00.000000', '2022-01-03 23:59:59.000000'])->get();
        // // 4
        // $get_orders = Order::where('order_note', 'active')->whereBetween('created_at',['2022-01-04 00:00:00.000000', '2022-01-04 23:59:59.000000'])->get();

        foreach($get_orders as $order){

            $data = json_decode($order->order_details,1);

            $cartTotal = $data['total_price'];
            $itemQty = array_sum(Arr::pluck($data['line_items'], 'quantity'));

            $pledgeValue = "de-active";
            $note_attr = $data['note_attributes'];

            $pos = isset($data['client_details']["user_agent"])?$data['client_details']["user_agent"]:NULL;

            if(count($note_attr) >= 1){
                foreach ($note_attr as $attribute){
                    if($attribute['name'] === "pledge")
                        $pledgeValue = $attribute['value'];
                }
            }else if(strpos($pos, 'POS') !== false) {
                $pledgeValue = "active";
                $pos = 1;
            }

            $findUser = User::where('id',$order->user_id)->first();

            \Log::info("findUser User ID :---". json_encode($findUser->api_user_id));

            $input = [
                'key' => @$findUser->api_user_id,
                'be' => @$data['email'],
                'on' => $data['id'],
                'don' => $data['order_number'],
                'qty' => @$itemQty,
                'pa' =>  @$cartTotal,
                'currency' => @$data['currency'],
                'pl' => in_array($pledgeValue,['active', 'packaging-free']) ? 1 : 0,
                'pf' => 'sho',
                'created_at' => $order->created_at->toDateTimeString(),
                'updated_at' => $order->updated_at->toDateTimeString()
            ];

            \Log::info("input Order Detail :---", $input);

            if(in_array($pledgeValue,['active', 'packaging-free'])){
                \Log::info('Calling apis---------');
                $response = Http::asForm()->withHeaders([
                    'Content-Type' => 'multipart/form-data',
                ])->post('https://dashboard.handprint.tech/ext/api/v4/save-order-manual', $input);
            }

            \Log::info($response->getBody());

        }

        return true;
    }
}
