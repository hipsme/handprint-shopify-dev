<?php

namespace App\Http\Controllers\Api\Widget;

use App\Http\Controllers\Controller;
use App\Models\User;

class WidgetController extends Controller
{
    public function index($shopifyShop)
    {
        $shop = User::whereName($shopifyShop)->where('status',1)->firstOrFail();
        $defaultSetting=[ 'button_position' => "Bottom Center" , 'cart_position' => "Cart Page" ];
        $loginData = [];
//        $loginData = loginApi();
//        if($loginData !== false) {
            $shop = [
                'id' => $shop->id,
                'api_user_id' => nullToEmpty($shop->api_user_id),
                'status' => nullToEmpty($shop->status),
                'api_login_token' => nullToEmpty($shop->api_login_token),
                'settings' => @$shop->setting->visual_guide?$shop->setting->visual_guide:$defaultSetting,
                'handprint_api_url' => config('app.handprint_api_url'),
                'login_data' => $loginData
            ];
//            }
        return $this->sendResponse($shop, 200);
    }
}
