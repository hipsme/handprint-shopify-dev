<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'visual_guide'
    ];

    protected $casts = [
        'visual_guide' => 'array'
    ];
}
